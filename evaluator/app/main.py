__author__ = 'Anushree'

import os
import sys
import pandas as pd
import argparse


if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark


from indexer.app.indexer import CustomStringIndexer
from indexer.app.indexer import LabelIndexer
from encoder.app.encoder import CustomerOneHotEncoderEstimator
from vector.app.assembler import CustomVectorAssembler
from pyspark.ml.classification import LogisticRegressionModel
from pyspark.ml.evaluation import BinaryClassificationEvaluator
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component \
    import XprPipeline



HDFS_BASE_PATH = "hdfs://172.16.1.81:8020/xpresso-data/projects/mixed_project"
HDFS_INPUT_TRAIN_DATA_LOCATION = HDFS_BASE_PATH + "/input/train"
HDFS_INPUT_TEST_DATA_LOCATION = HDFS_BASE_PATH + "/input/test"
HDFS_OUTPUT_MODEL_LOCATION = HDFS_BASE_PATH + "/output/model"
HDFS_OUTPUT_PREDICTIONS_LOCATION = HDFS_BASE_PATH + "/output/predictions"


class MyPipeline(XprPipeline):

    def __init__(self, sys_args=[]):
        XprPipeline.__init__(self, sys_args)

    @staticmethod
    def main(args):
        """
        Accepts the command line args and start the pipeline

        Arguments:
            args {sys.argv} -- all command line args
        
        Returns:
            [MyPipeline] -- object of current class `MyPipeline`
            [DataFrame] -- the data frame object of the input data read
        """
        sys_args = args[1:]
        pipeline = MyPipeline(sys_args=sys_args)
        pipeline_stages = pipeline.prepare_stages()
        pipeline.setStages(pipeline_stages)
        return pipeline, pipeline.start()

    def read_data(self, path, format, inferSchema, header, mode):
        data_df = self.spark_session.read.load(path,
            format=format,
            inferSchema=inferSchema,
            header=header,
            mode=mode)
        return data_df


    def start(self):
        """ 
        Starts the pipeline
        Returns:
            pyspark.sql.DataFrame -- pyspark dataframe read source path
        """

        path = HDFS_INPUT_TEST_DATA_LOCATION
        data_df = self.read_data(path, "csv", "true", "true", "DROPMALFORMED")
        data_df.printSchema()
        pm = self.fit(data_df)
        data_df = pm.transform(data_df)
        data_df.printSchema()
        print(f'Returning Test data...', flush=True)
        return data_df
    
    def prepare_stages(self):

        """This can be used prepare the stages of the pipeline
        
        Returns:
            list -- list of XprComponents; and will be stages in the [Xpr|My]Pipeline

        """

        categoricalColumns = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'poutcome']
        numericColumns = ['age', 'balance', 'duration', 'campaign', 'pdays', 'previous']

        pipeline_stages = []

        for categoricalCol in categoricalColumns:
            stringIndexer = CustomStringIndexer(categoricalCol+'-indexer', self.xpresso_run_name, inputCol = categoricalCol, outputCol = categoricalCol + 'Index')
            encoder = CustomerOneHotEncoderEstimator(categoricalCol+'-encoder', self.xpresso_run_name, inputCols=[stringIndexer.getOutputCol()], outputCols=[categoricalCol + "classVec"])
            pipeline_stages += [stringIndexer, encoder]

        label_stringIdx = LabelIndexer('labelindexer', self.xpresso_run_name, inputCol = 'deposit', outputCol = 'label')
        pipeline_stages += [label_stringIdx]

        assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
        assembler = CustomVectorAssembler('assembler', self.xpresso_run_name, inputCols=assemblerInputs, outputCol="features")
        assembler.setHandleInvalid("skip")
        pipeline_stages += [assembler]
        # pipeline stages design complete
        return pipeline_stages

    def stop(self):
        try:
            self.completed()
            self.spark_session.stop()
        except Exception as e:
            print(str(e))


if __name__ == "__main__":

    pipeline, df = MyPipeline.main(sys.argv)
    df.printSchema()
    
    model_path = f'{HDFS_OUTPUT_MODEL_LOCATION}{pipeline.xpresso_run_name}'
    rf_model = LogisticRegressionModel.load(model_path)

    predictions = rf_model.transform(df)
    predictions.select('age', 'job', 'label', 'rawPrediction', 'prediction', 'probability').show(10)
    # predictions.printSchema()

    print(f"\nTest Dataset Size: {df.count()}")

    path = f'{HDFS_OUTPUT_PREDICTIONS_LOCATION}{pipeline.xpresso_run_name}'
    pipeline.save_predictions(predictions, path)

    accuracy = 0
    loss = 1
    status = None
    roc=None
    try:
        evaluator = BinaryClassificationEvaluator()
        roc=evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderROC"})
        print("\n\nTest area under ROC: " + str(evaluator.evaluate(predictions, {evaluator.metricName: "areaUnderROC"})))
        loss = 1 - roc
        status = f'ROC: {roc} and loss: {loss}'
    except Exception as e:
        msg = f'Exception when evaluating model! {str(e)}'
        print(msg)
        status = str(msg)
    


    report_status1 = {
            "status": {
                "status": status
            },
            "accuracy": roc,
            "loss": loss
        }



    pipeline.report_status(report_status1)

    pipeline.stop()