from pyspark.ml.feature import VectorAssembler
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineTransformer


class CustomVectorAssembler(VectorAssembler, AbstractSparkPipelineTransformer):
    """ This is a pyspark implementation of Vector Assembler. It converts a
    It assmebles the vector present in input  cols and convert to output col """

    def __init__(self, name, xpresso_run_name, inputCols=None, outputCol=None):
        class_name = self.__class__.__name__
        VectorAssembler.__init__(self,
                                 inputCols=inputCols,
                                 outputCol=outputCol)
        AbstractSparkPipelineTransformer.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name = 'vector'

    def _transform(self, dataset):
        """ Initialize the state of the run and executing the data processing
        using parent Vector Assmbler class
        """
        self.state = dataset
        self.start(self.xpresso_run_name)
        ds = super()._transform(dataset)
        self.state = ds
        return ds