__author__ = 'Anushree'

import os
import sys
import pandas as pd
import argparse


if os.path.exists('packs.zip'):
    sys.path.insert(0, 'packs.zip')

try:
    import pyspark
except:
    import findspark
    findspark.init()
    import pyspark


from indexer.app.indexer import CustomStringIndexer
from indexer.app.indexer import LabelIndexer
from encoder.app.encoder import CustomerOneHotEncoderEstimator
from vector.app.assembler import CustomVectorAssembler
from pyspark.ml.classification import LogisticRegression
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component \
    import XprPipeline



HDFS_BASE_PATH = "hdfs://172.16.1.81:8020/xpresso-data/projects/mixed_project"
HDFS_INPUT_TRAIN_DATA_LOCATION = HDFS_BASE_PATH + "/input/train"
HDFS_INPUT_TEST_DATA_LOCATION = HDFS_BASE_PATH + "/input/test"
HDFS_OUTPUT_MODEL_LOCATION = HDFS_BASE_PATH + "/output/model"
HDFS_OUTPUT_PREDICTIONS_LOCATION = HDFS_BASE_PATH + "/output/predictions"

class MyPipeline(XprPipeline):

    def __init__(self, sys_args=[]):
        """ Initializes parent pipeline component to initialize the state """
        XprPipeline.__init__(self, sys_args)

    @staticmethod
    def main(args):
        """
        Accepts the command line args and starts the pipeline

        Arguments:
            args {sys.argv} -- all command line args

        Returns:
            [MyPipeline] -- object of current class `MyPipeline`
            [DataFrame] -- the data frame object of the input data read
        """

        sys_args = args[1:]
        pipeline = MyPipeline(sys_args=sys_args)
        pipeline_stages = pipeline.prepare_stages()
        pipeline.setStages(pipeline_stages)
        return pipeline, pipeline.start()

    def read_data(self, path, format, inferSchema, header, mode):
        """ Uses spark session to read the pyspark dataframe object """
        data_df = self.spark_session.read.load(path,
                                               format=format,
                                               inferSchema=inferSchema,
                                               header=header,
                                               mode=mode)
        return data_df

    def start(self):
        """
        Starts the pipeline
        Returns:
            pyspark.sql.DataFrame -- pyspark dataframe read source path
        """

        path = HDFS_BASE_PATH
        data_df = self.read_data(path, "csv", "true", "true", "DROPMALFORMED")
        data_df.printSchema()
        pm = self.fit(data_df)
        data_df = pm.transform(data_df)
        data_df.printSchema()
        print(f'Returning data_df...', flush=True)
        pd.DataFrame(data_df.take(5), columns=data_df.columns).transpose()

        return data_df

    def prepare_stages(self):
        categoricalColumns = ['job', 'marital', 'education', 'default', 'housing', 'loan', 'contact', 'poutcome']
        numericColumns = ['age', 'balance','duration', 'campaign', 'pdays', 'previous']

        pipeline_stages = []

        for categoricalCol in categoricalColumns:
            stringIndexer = CustomStringIndexer(categoricalCol + '-indexer', self.xpresso_run_name,
                                                inputCol=categoricalCol, outputCol=categoricalCol + 'Index')
            encoder = CustomerOneHotEncoderEstimator(categoricalCol + '-encoder', self.xpresso_run_name,
                                                     inputCols=[stringIndexer.getOutputCol()],
                                                     outputCols=[categoricalCol + "classVec"])
            pipeline_stages += [stringIndexer, encoder]

        assemblerInputs = [c + "classVec" for c in categoricalColumns] + numericColumns
        assembler = CustomVectorAssembler('assembler', self.xpresso_run_name, inputCols=assemblerInputs,
                                          outputCol="features")

        label_stringIdx = LabelIndexer('labelindexer', self.xpresso_run_name, inputCol='deposit', outputCol='label')
        pipeline_stages += [label_stringIdx]

        assembler.setHandleInvalid("skip")
        pipeline_stages += [assembler]
        # pipeline stages design complete
        return pipeline_stages



    def stop(self):
        """ This is call to stop the pipeline. It persists the existing state of
        the pipeline"""
        try:
            self.pipeline_completed()
            self.spark_session.stop()
        except Exception as e:
            print(str(e))


if __name__ == "__main__":
    """ Main file to start the execution    
    It takes x parameters from command line"""

    parser = argparse.ArgumentParser()
    parser.add_argument('--model_param_maxIteration', type=int, default=5,
                        help='model_param_numTrees help')

    args, unknown = parser.parse_known_args(sys.argv[1:])

    # start the pipeline
    pipeline, df = MyPipeline.main(sys.argv)

    # infomational print
    df.printSchema()
    print(f"\nTraining Dataset Size: {df.count()}")

    # build the model
    max_iter=args.model_param_maxIteration
    lr = LogisticRegression(featuresCol='features', labelCol='label', maxIter=max_iter)
    lrModel = lr.fit(df)
    trainingSummary = lrModel.summary
    print('Summary of training the model.....\n',trainingSummary)


    path = f'{HDFS_OUTPUT_MODEL_LOCATION}{pipeline.xpresso_run_name}'
    pipeline.save_model(lrModel, path)
    report_status1 = {
        "status": {
            "status": "Model"
        },
        "metric":{
            "Saved At": f"{path}"
        }
    }
    pipeline.report_status(report_status1)

    # MANDATORY - stop the pipeline
    pipeline.stop()
