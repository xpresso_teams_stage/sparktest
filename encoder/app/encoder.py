from pyspark.ml.feature import OneHotEncoderEstimator
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomerOneHotEncoderEstimator(OneHotEncoderEstimator,
                                     AbstractSparkPipelineEstimator):
    """ This is a pyspark implementation of One Hot Encoder. It converts a
    single column into  multiple columns with 0/1 values """

    def __init__(self, name, xpresso_run_name,
                 inputCols=None, outputCols=None,
                 handleInvalid='error', dropLast=True):
        OneHotEncoderEstimator.__init__(self, inputCols=inputCols,
                                        outputCols=outputCols,
                                        handleInvalid=handleInvalid,
                                        dropLast=dropLast)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name = 'encoder'

    def _fit(self, dataset):
        """ Initialize the state of the run and executing the data processing
        using parent OneHotEncoder class
        """
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model