"""
This is a custom StringIndexer

"""

import os

from pyspark.ml.feature import StringIndexer
from xpresso.ai.core.data.pipeline.abstract_spark_pipeline_component import \
    AbstractSparkPipelineEstimator


class CustomStringIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    """ This is a pyspark implementation of String Indexer. It converts a
    single column to an index column """

    def __init__(self, name, xpresso_run_name,
                 inputCol=None, outputCol=None,
                 handleInvalid='skip', stringOrderType='frequencyDesc'):
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol,
                               handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name="indexer"

    def _fit(self, dataset):
        """ Initialize the state of the run and executing the data processing
        using parent StringIndexer class
        """
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model


class LabelIndexer(StringIndexer, AbstractSparkPipelineEstimator):
    """ This is a pyspark implementation of String Indexer. It converts a
    Labels in the dataset to an index column """

    def __init__(self, name, xpresso_run_name,
                 inputCol=None, outputCol=None,
                 handleInvalid='skip', stringOrderType='frequencyDesc'):
        StringIndexer.__init__(self, inputCol=inputCol, outputCol=outputCol,
                               handleInvalid=handleInvalid, stringOrderType=stringOrderType)
        AbstractSparkPipelineEstimator.__init__(self, name=name)
        self.xpresso_run_name = xpresso_run_name
        self.name = "indexer"

    def _fit(self, dataset):
        """ Initializatin the state of the run and executing the data processing
        using parent StringIndexer class
        """
        self.state = dataset
        self.start(self.xpresso_run_name)
        model = super()._fit(dataset)
        return model